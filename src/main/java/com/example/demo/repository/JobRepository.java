package com.example.demo.repository;

import com.example.demo.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface JobRepository extends JpaRepository<Job, Long> {
    Job findByIdAndUserId(Long jobId, Long userId);

    List<Job> findByNextFireTimeBefore(Date date);
}
