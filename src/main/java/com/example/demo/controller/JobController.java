package com.example.demo.controller;

import com.example.demo.model.dto.JobDto;
import com.example.demo.service.JobService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/job")
public class JobController {

    private final JobService jobService;

    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody JobDto jobDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(jobService.create(jobDto));
    }

    @GetMapping()
    public ResponseEntity getJobs() {
        return ResponseEntity.ok(jobService.getJobs());
    }

    @PutMapping()
    public ResponseEntity updateJob(@RequestBody JobDto jobDto) {
        return ResponseEntity.ok(jobService.update(jobDto));
    }

    @DeleteMapping(value = "/{jobId}")
    public ResponseEntity deleteJob(@PathVariable Long jobId) {
        jobService.delete(jobId);
        return ResponseEntity.ok().build();
    }
}
