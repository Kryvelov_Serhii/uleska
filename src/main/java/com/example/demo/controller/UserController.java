package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping()
    public ResponseEntity register(@RequestBody @Valid User user) {
        if (userService.isUserExistsByEmail(user.getEmail())) {
            return ResponseEntity.badRequest().body("User with such email already exists");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user));
    }
}
