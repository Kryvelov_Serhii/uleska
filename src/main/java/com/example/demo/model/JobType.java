package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class JobType {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @OneToMany(
            mappedBy = "jobType",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Job> jobs;
}
