package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Job {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column
    private String name;

    @Column
    private Date nextFireTime;

    @ManyToOne(fetch = FetchType.LAZY)
    private JobType jobType;

    @Column
    private String state;

    @Column
    private Long frequency;

    @Column
    private String data;

    @OneToMany(
            mappedBy = "job",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<JobHistory> jobHistories;
}
