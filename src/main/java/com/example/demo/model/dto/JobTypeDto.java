package com.example.demo.model.dto;

import lombok.Data;

@Data
public class JobTypeDto {
    private Long id;
    private String name;
}
