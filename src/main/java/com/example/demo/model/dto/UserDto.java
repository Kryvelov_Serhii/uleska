package com.example.demo.model.dto;

import com.example.demo.model.RoleEnum;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserDto {
    private Long id;
    private String email;
    private String name;
    private Boolean isActive;
    private RoleEnum role;
    private Date creationDate;
    private List<JobDto> jobDtos;
}
