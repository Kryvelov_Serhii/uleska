package com.example.demo.model.dto;

import lombok.Data;

import java.util.Date;

@Data
public class JobHistoryDto {
    private Long id;
    private JobDto job;
    private Date start_time;
    private String result;
    private String status;

}
