package com.example.demo.model.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class JobDto {
    private Long id;
    private String name;
    private Date nextFireTime;
    private JobTypeDto jobTypeDto;
    private String state;
    private Long frequency;
    private String data;
    private List<JobHistoryDto> jobHistoryDtos;
}
