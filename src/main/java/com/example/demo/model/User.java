package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

@Data
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @GeneratedValue
    @Id
    private Long id;

    @Column(unique = true)
    @NotEmpty
    private String email;

    @Column
    private String name;

    @Column
    @NotEmpty
    private String password;

    @Column
    private Boolean isActive;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    @Column
    private Date creationDate;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    private List<Job> jobs;
}
