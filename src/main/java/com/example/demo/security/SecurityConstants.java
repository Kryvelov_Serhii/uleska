package com.example.demo.security;

public class SecurityConstants {
    public static final String SECRET = "TestTaskSecret";
//    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final long EXPIRATION_TIME = 86400000; // 1 day
//    public static final long EXPIRATION_TIME = 3600000L; // 1 hour
//    public static final long EXPIRATION_TIME = 300000L; // 5 mins
//    public static final long EXPIRATION_TIME = 10000L; // 10 secs
//    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String REFRESH_TOKEN = "Refresh_token";
}
