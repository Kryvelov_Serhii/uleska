package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.model.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserDto save(User user);

    boolean isUserExistsByEmail(String email);
}
