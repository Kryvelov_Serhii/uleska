package com.example.demo.service.mapper;

import com.example.demo.model.User;
import com.example.demo.model.dto.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserUserDtoMapper {

    UserDto userToUserDto(User user);
    User userDtoToUser(UserDto userDto);
}
