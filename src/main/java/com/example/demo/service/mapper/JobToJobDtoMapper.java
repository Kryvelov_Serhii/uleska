package com.example.demo.service.mapper;

import com.example.demo.model.Job;
import com.example.demo.model.JobType;
import com.example.demo.model.dto.JobDto;
import com.example.demo.model.dto.JobTypeDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface JobToJobDtoMapper {

    JobTypeDto jobTypeToJobTypeDto(JobType jobType);
    JobType jobTypeDtoToJobType(JobTypeDto jobTypeDto);

    @Mapping(target = "jobTypeDto", source = "jobType")
    JobDto jobToJobDto(Job job);
    @Mapping(target = "jobType", source = "jobTypeDto")
    Job jobDtoToJob(JobDto jobDto);


}
