package com.example.demo.service;

import com.example.demo.model.dto.JobDto;

import java.util.List;

public interface JobService {

    JobDto create(JobDto jobDto);

    List<JobDto> getJobs();

    JobDto update(JobDto jobDto);

    void delete(Long jobId);
}
