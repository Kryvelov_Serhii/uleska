package com.example.demo.service;

import com.example.demo.model.Job;

import java.io.IOException;

public interface SystemCallService {

    String runSystemCommand(Job job) throws IOException, InterruptedException;

    String runJavaCode(Job job) throws IOException, InterruptedException;
}
