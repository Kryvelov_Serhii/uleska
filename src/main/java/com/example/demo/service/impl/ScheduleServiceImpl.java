package com.example.demo.service.impl;

import com.example.demo.model.Job;
import com.example.demo.model.JobHistory;
import com.example.demo.model.JobType;
import com.example.demo.repository.JobHistoryRepository;
import com.example.demo.repository.JobRepository;
import com.example.demo.service.ScheduleService;
import com.example.demo.service.SystemCallService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    private final JobRepository jobRepository;
    private final JobHistoryRepository jobHistoryRepository;
    private final SystemCallService systemCallService;

    public ScheduleServiceImpl(JobRepository jobRepository, JobHistoryRepository jobHistoryRepository, SystemCallService systemCallService) {
        this.jobRepository = jobRepository;
        this.jobHistoryRepository = jobHistoryRepository;
        this.systemCallService = systemCallService;
    }

    @Override
    @Scheduled(fixedRate = 10000)
    @Transactional
    public void runJobs() {
        List<Job> jobs = jobRepository.findByNextFireTimeBefore(new Date());
        if (jobs == null)
            return;
        jobs.forEach(job -> {
            JobHistory jobHistory = new JobHistory();
            jobHistory.setJob(job);
            jobHistory.setStart_time(new Date());
            if (job.getState().equals("stopped")) {
                jobHistory.setResult(null);
                jobHistory.setStatus("stopped");
            }
            JobType jobType = job.getJobType();
            if (jobType.getName().equals("print_statement")) {
                jobHistory.setResult(job.getData());
                jobHistory.setStatus("done");
            }
            if (jobType.getName().equals("system_call")) {
                try {
                    jobHistory.setResult(systemCallService.runSystemCommand(job));
                    jobHistory.setStatus("done");
                } catch (Exception e) {
                    jobHistory.setStatus("error");
                }
            }
            if (jobType.getName().equals("execute_code")) {
                try {
                    jobHistory.setResult(systemCallService.runJavaCode(job));
                    jobHistory.setStatus("done");
                } catch (Exception e) {
                    jobHistory.setStatus("error");
                }
            }
            jobHistoryRepository.save(jobHistory);
            job.setNextFireTime(new Date(System.currentTimeMillis() + job.getFrequency()));
        });
    }

}
