package com.example.demo.service.impl;

import com.example.demo.model.RoleEnum;
import com.example.demo.model.User;
import com.example.demo.model.UserDetailsImpl;
import com.example.demo.model.dto.UserDto;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import com.example.demo.service.mapper.UserUserDtoMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService {


    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserUserDtoMapper userUserDtoMapper;

    public UserServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepository, UserUserDtoMapper userUserDtoMapper) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.userUserDtoMapper = userUserDtoMapper;
    }

    @Override
    public UserDto save(User user) {
        user.setCreationDate(new Date());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setIsActive(true);
        user.setRole(RoleEnum.USER);
        user = userRepository.save(user);
        return userUserDtoMapper.userToUserDto(user);
    }

    @Override
    public boolean isUserExistsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        if (!user.getIsActive()) {
            throw new UsernameNotFoundException(email);
        }
        return new UserDetailsImpl(user);
    }
}
