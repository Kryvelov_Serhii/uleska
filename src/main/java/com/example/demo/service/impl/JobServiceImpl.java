package com.example.demo.service.impl;

import com.example.demo.model.Job;
import com.example.demo.model.User;
import com.example.demo.model.dto.JobDto;
import com.example.demo.repository.JobRepository;
import com.example.demo.repository.JobTypeRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.JobService;
import com.example.demo.service.mapper.JobToJobDtoMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JobServiceImpl implements JobService {

    private final JobRepository jobRepository;
    private final JobToJobDtoMapper jobToJobDtoMapper;
    private final JobTypeRepository jobTypeRepository;
    private final UserRepository userRepository;


    public JobServiceImpl(JobRepository jobRepository, JobToJobDtoMapper jobToJobDtoMapper, JobTypeRepository jobTypeRepository, UserRepository userRepository) {
        this.jobRepository = jobRepository;
        this.jobToJobDtoMapper = jobToJobDtoMapper;
        this.jobTypeRepository = jobTypeRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public JobDto create(JobDto jobDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(String.valueOf(auth.getPrincipal()));
        if (user == null) {
            throw new RuntimeException("user not found");
        }

        if (jobDto.getJobTypeDto() == null ||
                jobDto.getJobTypeDto().getId() == null ||
                !jobTypeRepository.existsById(jobDto.getJobTypeDto().getId())) {
            throw new RuntimeException("job type should be not null");
        }
        Job job = jobToJobDtoMapper.jobDtoToJob(jobDto);
        job.setUser(user);
        job.setNextFireTime(new Date(System.currentTimeMillis() + job.getFrequency()));
        return jobToJobDtoMapper.jobToJobDto(jobRepository.save(job));
    }

    @Override
    @Transactional
    public List<JobDto> getJobs() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(String.valueOf(auth.getPrincipal()));
        if (user == null) {
            throw new RuntimeException("user not found");
        }
        return user.getJobs().stream().map(jobToJobDtoMapper::jobToJobDto).collect(Collectors.toList());
    }

    @Override
    public JobDto update(JobDto jobDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(String.valueOf(auth.getPrincipal()));
        if (user == null) {
            throw new RuntimeException("user not found");
        }
        Job job = jobToJobDtoMapper.jobDtoToJob(jobDto);
        if (!jobRepository.existsById(job.getId())) {
            throw new RuntimeException("job not found");
        }

        return jobToJobDtoMapper.jobToJobDto(jobRepository.save(job));
    }

    public void delete(Long jobId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(String.valueOf(auth.getPrincipal()));
        if (user == null) {
            throw new RuntimeException("user not found");
        }
        Job job = jobRepository.findByIdAndUserId(jobId, user.getId());
        if (job != null) {
            jobRepository.delete(job);
        }
    }
}
