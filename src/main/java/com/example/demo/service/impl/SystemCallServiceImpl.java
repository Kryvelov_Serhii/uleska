package com.example.demo.service.impl;

import com.example.demo.model.Job;
import com.example.demo.service.SystemCallService;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Service
public class SystemCallServiceImpl implements SystemCallService {

    private static final boolean IS_WINDOWS = System.getProperty("os.name")
            .toLowerCase().startsWith("windows");

    @Override
    public String runSystemCommand(Job job) throws IOException {

        ProcessBuilder builder = new ProcessBuilder();
        if (IS_WINDOWS) {
            builder.command("cmd.exe", "/c", job.getData());
        } else {
            builder.command("sh", "-c", job.getData());
        }
        Process process = builder.start();
        return new BufferedReader(
                new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
    }

    @Override
    public String runJavaCode(Job job) throws IOException {

        Path file = Paths.get(job.getName() + ".java");
        Files.write(file, job.getData().getBytes(StandardCharsets.UTF_8));

        ProcessBuilder builder = new ProcessBuilder();
        if (IS_WINDOWS) {
            builder.command("cmd.exe", "/c", "java " + file.getFileName());
        } else {
            builder.command("sh", "-c", "java " + file.getFileName());
        }
        Process process = builder.start();
        return new BufferedReader(
                new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8))
        .lines()
                .collect(Collectors.joining("\n"));
    }
}
